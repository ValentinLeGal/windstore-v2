<?php

namespace App\Form;

use App\Entity\LogicielSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LogicielSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required'  =>  true,
                'label'     =>  false,
                'attr'      =>  [
                    'autofocus'     =>  true,
                    'placeholder'   =>  'ex : Mozilla Firefox'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'        =>  LogicielSearch::class,
            'method'            =>  'get',
            'csrf_protection'   =>  false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
