<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Developpeur;
use App\Entity\Logiciel;
use App\Entity\LogicielSearch;
use App\Form\ContactType;
use App\Form\LogicielSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Categorie::class)->findBy(
            [],                 // Critère
            ['nom'  =>  'ASC'], // Ordre
            5                   // Limite
        );

        $logiciels = $em->getRepository(Logiciel::class)->findBy(
            [],                 // Critère
            ['id' => 'DESC'],   // Ordre
            5                   // Limite
        );

        $search = new LogicielSearch();
        $form = $this->createForm(LogicielSearchType::class, $search);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) :
            $results = $em->getRepository(Logiciel::class)->findBy(
                ['nom'  =>  $search->getNom()], // Critère
                ['nom'  =>  'ASC']              // Ordre
            );

            return $this->render('default/more.html.twig', [
                'name'      =>  $search->getNom(),
                'entity'    =>  $results,
                'search'    =>  true,
                'dev'       =>  false,
                'soft'      =>  true
            ]);
        endif;

        return $this->render('default/index.html.twig', [
            'categories'    =>  $categories,
            'logiciels'     =>  $logiciels,
            'form'          =>  $form->createView()
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('default/about.html.twig', []);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) :
            if($this->sendEmail($form->getData(),$mailer)) :
                $this->addFlash(
                    'notice',
                    'Votre message a été envoyé !'
                );
            else :
                $this->addFlash(
                    'notice',
                    'une erreur est apparue'
                );
            endif;
            return $this->redirect($request->getUri());
        endif;

        return $this->render('default/contact.html.twig', ['form' => $form->createView()]);
    }

    private function sendEmail($data, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message())
            ->setSubject($data["nom"])
            ->setFrom($data["email"])
            ->setTo("valentin.le_gal@etudiant.univ-lr.fr")
            ->setBody($data["message"])
        ;

        return $mailer->send($message);
    }

    /**
     * @Route("/signup", name="signup")
     */
    public function signup()
    {
        return $this->render('default/signup.html.twig', []);
    }

    /**
     * @Route("/software/{id}",requirements={"id": "[1-9]\d*"}, name="software")
     */
    public function software($id)
    {
        $em = $this->getDoctrine()->getManager();

        $logiciel = $em->getRepository(Logiciel::class)->find($id);

        return $this->render('default/software.html.twig', ['logiciel' => $logiciel]);
    }

    /**
     * @Route("/categories", name="categories")
     */
    public function categories()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Categorie::class)->findBy(
            [],                 // Critère
            ['nom'  =>  'ASC']  // Ordre
        );

        return $this->render('default/more.html.twig', [
            'name'      =>  'Catégories',
            'entity'    =>  $categories,
            'search'    =>  false,
            'dev'       =>  false,
            'soft'      =>  false
        ]);
    }

    /**
     * @Route("/categorie/{id}", requirements={"id": "[1-9]\d*"}, name="listByCat")
     */
    public function listByCat($id)
    {
        $em = $this->getDoctrine()->getManager();

        $categorie = $em->getRepository(Categorie::class)->findOneBy(['id' => $id]);

        $logiciels = $em->getRepository(Logiciel::class)->findBy(
            ['categorie'    =>  $id],   // Critère
            ['nom'          =>  'ASC']  // Ordre
        );

        return $this->render('default/more.html.twig', [
            'name'      =>  $categorie,
            'entity'    =>  $logiciels,
            'search'    =>  false,
            'dev'       =>  false,
            'soft'      =>  true
        ]);
    }

    /**
     * @Route("/dev/{id}", requirements={"id": "[1-9]\d*"}, name="listByDev")
     */
    public function listByDev($id)
    {
        $em = $this->getDoctrine()->getManager();

        $developpeur = $em->getRepository(Developpeur::class)->findOneBy(['id' => $id]);

        $logiciels = $em->getRepository(Logiciel::class)->findBy(
            ['developpeur'  =>  $id],   // Critère
            ['nom'          =>  'ASC']  // Ordre
        );

        return $this->render('default/more.html.twig', [
            'name'      =>  $developpeur,
            'entity'    =>  $logiciels,
            'search'    =>  false,
            'dev'       =>  true,
            'soft'      => true
        ]);
    }

    /**
     * @Route("/recents", name="recents")
     */
    public function recents()
    {
        $em = $this->getDoctrine()->getManager();

        $logiciels = $em->getRepository(Logiciel::class)->findBy(
            [],                 // Critère
            ['id'   =>  'DESC'] // Ordre
        );

        return $this->render('default/more.html.twig', [
            'name'      =>  'Récemments ajoutés',
            'entity'    =>  $logiciels,
            'search'    =>  false,
            'dev'       =>  false,
            'soft'      =>  true
        ]);
    }
}
