# WindStore V2

Un site web développé, à l'aide de Symfony 4, permettant de télécharger la dernière version d'un logiciel Windows à partir de son lien officiel. Il s'agit de mon troisième site web développé à l'occasion d'un projet universitaire (2019).

Ce projet fait suite à [mon premier site web](https://gitlab.com/ValentinLeGal/windstore). Il s'agit de la version que j'aurais voulu développer dès le départ lorsque je ne savais pas encore développer de site web dynamique.

![WindStore cover](./public/img/cover.png)

## À propos du projet

- **Nature :** projet universitaire
- **Date :** mars 2019
- **Description :** il s'agit d'un site web permettant le téléchargement d'un logiciel Windows (dernière version et lien officiel). L'utilisateur est ainsi protégé contre tout type de piratage en pensant télécharger un logiciel alors qu'il s'agit d'un virus. Les administrateurs peuvent se connecter à un tableau de bord. Ce dernier permet l'ajout, la modification et la suppression de données sur le site (logiciel, éditeur, licence, etc.). À noter qu'une API est générée à partir des données contenues dans la base de données

## Technologies utilisées

- Symfony 4
- PHP
- SQL
- HTML
- CSS
- JS

## En savoir plus

Pour en savoir plus, un PDF nommé `compte_rendu.pdf` est disponible dans ce répertoire.

## Information supplémentaire

Pour accéder au tableau de bord des administrateurs :
- **Email :** admin@windstore.fr
- **Mot de passe :** admin